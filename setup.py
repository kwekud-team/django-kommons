import setuptools

long_description = ''
try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except FileNotFoundError:
    pass

setuptools.setup(
     name='kommons',
     version='0.1.51',
     author="KDanso",
     author_email="dev@kkdanso.com",
     description="Variety of relevant tools to simplify development",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://bitbucket.org/kwekud-team/django-kommons",
     packages=setuptools.find_packages(),
     include_package_data=True,
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
     install_requires=[
         'python-dateutil',
         'tablib',
         'xlwt',
         'phonenumbers',
     ]
)