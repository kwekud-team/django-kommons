# -*- coding: utf-8 -*-
from django import template
from django.core.exceptions import FieldDoesNotExist
from django.utils.safestring import mark_safe
from django.template.defaultfilters import yesno, date, capfirst, linebreaks, truncatechars
from django.contrib.humanize.templatetags.humanize import intcomma

from kommons.utils.dates import format_date_time
from kommons.utils.model import get_recursive_field

register = template.Library()

from kommons.utils import generic
from kommons.utils.generic import monefy


@register.inclusion_tag("includes/fieldset_details.html", takes_context=True) 
def display_fields_tag(context, obj, fields_list=None):
    user = context.get('user', None)
    
    context.update({'display_fields': get_display_fields(obj, fields_list or [], user=user)})

    return context


@register.filter  
def get_display_field(obj, field):
    return get_display_fields(obj, [field])[0]

@register.filter  
def get_display_labels(obj, fields_listo=None):
    fields = []        
    if obj:
        # Hack to force field_types for functions 
        attrs = getattr(obj, 'Attr', None)
        field_types = getattr(attrs, 'field_types', {})
        
        for cnt, field_name in enumerate(fields_listo or fields_list(obj)):
            new_obj = obj
            field_type = None
            vals = get_recursive_field(field_name, obj=obj)
            field = vals['field']
            new_obj = vals['obj'] or obj
            
            if field:   
                field_name = vals['field_name'] 
                field_type = field.get_internal_type()
                label = str(field.verbose_name)
            else:  
                if field_name[:3] == "___":
                    label = field_name 
                else:
                    try:
                        field_name = vals['field_name']
                        label, value, field_type = display_function(new_obj, field_name, only_label=True)
                    except:
                        raise Exception('field name does not exist: %s' % field_name)   
        
            fields.append({
                'label': capfirst(label),
                'field_type': field_type,
                'field_name': field_name
            })
    return fields


@register.filter  
def get_display_fields(obj, fields_listo=None, tags=False, user=None):
    special_fields = {'DateField': display_date,
                      'DateTimeField': display_datetime,
                      'BooleanField': display_boolean,
                      'URLField': display_url,
                      'FileField': display_file,
                      'ForeignKey': display_foreign,
                      'DecimalField': display_decimal,
                      'IntegerField': display_integer,
                      'ChainedForeignKey': display_foreign,
                      'OneToOneField': display_foreign,
                      'ManyToManyField': display_many,
                      'TextField': display_text,
                      # 'TextField': display_text
                    }    
    fields = []        
    if obj:
        # Hack to force field_types for functions 
        attrs = getattr(obj, 'Attr', None)
        field_types = getattr(attrs, 'field_types', {})
        
        for cnt, field_name in enumerate(fields_listo or fields_list(obj)):
            new_obj = obj
            field_type = None
            vals = get_recursive_field(field_name, obj=obj)
            field = vals['field']
            new_obj = vals['obj'] or obj
            new_model_class = vals['model_class'] 
            
            if field:   
                field_name = vals['field_name'] 
                field_type = field.get_internal_type()
                label = str(field.verbose_name)
                try:
                    cls_name = field.__class__.__name__
                except:
                    cls_name = field_type

                try:
                    valid_obj = str(new_obj)
                except:
                    valid_obj = None
                    
                if valid_obj:
                    # if field_type in special_fields:
                    if cls_name in special_fields:
                        func = special_fields[cls_name]
                        value = func(new_obj, field_name, user)
                    else:
                        value = display_default(new_obj, field_name)
                else:
                    value = '-'
            else:  
                if field_name[:3] == "___":
                    label = field_name
                    value = field_name.replace('_', '')
                else: 
                    try:
                        field_name = vals['field_name']
                        label, value, field_type = display_function(new_obj, field_name)  
                    except FieldDoesNotExist: 
                        raise Exception('field name does not exist: %s %s' % (new_model_class, field_name))
            
            first_link = False
            absolute_url = getattr(new_obj, 'get_absolute_url', None) 
            absolute_url = (absolute_url() if absolute_url else None)
            if cnt is 0 and absolute_url:
                first_link = True
            
            # Hack to not capitalize email value
            if field_type in ['EmailField'] or '@' in str(value):
                value = mark_safe(force_none(value))
            else:
                value = capfirst(mark_safe(force_none(value))) 
                
            #if field_name in field_types: 
            if field_name in field_types:
                func = special_fields[field_types[field_name]]
                value = func(new_obj, field_name, user) 
        
            fields.append({'value': value or '-', 
                           'label': capfirst(label),
                           'first_link': first_link, 
                           'field_type': field_type,
                           'field_name': field_name})             
    return fields


def get_recursive_value(obj, field_name): 
    val = obj 
    for x in field_name.split('__'):
        val = getattr(val, x) 
    return val

def force_none(val, default=''):
    if not val or val == 'None':
        return default
    else:
        return val
    
@register.filter
def get_verbose(obj, field_name):
    field = obj._meta.get_field(field_name) 
    return field.verbose_name.decode()

def fields_list(obj):
    no_display = ['id', 'slug', 'password', 'groups', 'user_permissions']
    
    display = getattr(getattr(obj, 'Attr', None), 'list_display', None) or []
     
    if display:
        return display
    else:
        return [x for x in [y.name for y in obj._meta.fields if y.editable] 
                if x not in no_display]
        
def get_field(obj, field_name):
    n = field_name.split('.')
    try:
        f = obj._meta.get_field(n[0])
        return obj, f
    except FieldDoesNotExist:
        f = obj._meta.get_field_by_name(n[0])[0] 
        return getattr(obj, n[0], None), f.opts.get_field(n[1])
    except:
        raise (FieldDoesNotExist, '%s has no field named %r' % (obj.object_name, 
                                                               field_name))

@register.filter      
def display_default(obj, field_name): 
    try:
        return getattr(obj, 'get_%s_display' % field_name)()
    except:
        return getattr(obj, field_name, '')


def display_text(obj, field_name, user=None):
    if getattr(obj, field_name, ''):
        return linebreaks(getattr(obj, field_name, ''))


def display_date(obj, field_name, user=None):
    return date(getattr(obj, field_name, ''))


def display_datetime(obj, field_name, user=None):
    return format_date_time(getattr(obj, field_name, ''))


def display_url(obj, field_name, user=None):
    val = getattr(obj, field_name, '')
    return '<a href="%s">%s</a>' % (val, truncatechars(val, 30))


def display_decimal(obj, field_name, user=None): 
    attrs = getattr(obj, 'Attr', None)
    field_attrs = getattr(attrs, 'field_attrs', {})
    if field_attrs:
        f = field_attrs.get(field_name, {})
        decimal_places = f.get('decimal_places', 2)
    else:
        decimal_places = 2 
    val = getattr(obj, field_name, 0)
    return monefy(val, decimal_places)


def display_integer(obj, field_name, user=None):
    val = getattr(obj, field_name, 0)
    return intcomma(generic.intify(val))


def display_many(obj, field_name, user=None):
    vals = (getattr(obj, field_name, []))
    if vals:
        vals = [str(x) for x in vals.all()]
        return ', '.join(vals)
    return ''


def display_foreign(obj, field_name, user=None):
    j = obj
    for lvl in field_name.split('.'):
        j = getattr(j, lvl, '')
    
    val = j
    if j:
        app = j._meta.app_label
        model = j._meta.model_name
        perm = '%s.view_%s' % (app, model)
        if user:
            if user.has_perm(perm):
                val = generic.url_link(j) 
    return val
    
def display_boolean(obj, field_name, user=None):
    value = display_default(obj, field_name)
    return yesno(value)
    # return mark_safe('<img src="%s/img/admin/icon-%s.gif" alt=%s />' % ('/media/admin', yesno(value), yesno(value)))


def display_file(obj, field_name, user=None): 
    value = display_default(obj, field_name) 
    if value:        
        filename = value.name.split("/")[-1] 
        if check_imagefield(value):
            # Display images
            try:
                # remote storage gives issues with checking height
                if value.height > 100:
                    height = 100
                else:
                    height = value.height
            except:
                height = 100
            img = mark_safe('<img src="%s" alt="%s" height="%s" />' % (value.url, filename, height))
            return mark_safe('<a href="%s">%s</a>' % (value.url, img))
        else:
            # Display normal files 
            return mark_safe('<a href="%s">%s</a>' % (value.url, filename))
    return None
    

def display_function(obj, func_name, user=None, only_label=False): 
    func = getattr(obj, func_name, None) 
    if func: 
        label = getattr(func, 'short_description', '') or func_name.replace('_', ' ')
        field_type = getattr(func, 'field_type', '')
        if only_label:
            val = None
        else:
            try:
                val = func()
            except TypeError:
                # for property functions
                val = func or 0    
        return label, generic.url_link(val), field_type
    else:
        raise FieldDoesNotExist  
     
     
def check_imagefield(value):
    from django.db.models.fields.files import ImageFieldFile
    if type(value) == ImageFieldFile:
        return True
    