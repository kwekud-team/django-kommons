from django import template
from django.templatetags.static import static
from django.utils.safestring import mark_safe
from kommons.utils.generic import monefy, intify

register = template.Library()


@register.filter
def boolean_icon(value):
    img_src = ('admin/img/icon-yes.svg' if value else 'admin/img/icon-no.svg')
    return mark_safe(f'<img src="{static(img_src)}" />')


@register.filter
def show_money(amount, decimal_places=2):
    return monefy(amount, decimal_places=decimal_places)


@register.filter(name='intify')
def do_intify(value):
    return intify(value)


@register.filter
def get_dict_item(dictionary, key):
    return dictionary.get(key)


@register.filter('klass')
def klass(obj):
    return obj.__class__.__name__


@register.filter
def replace(value, arg):
    if len(arg.split('|')) != 2:
        return value

    what, to = arg.split('|')
    return value.replace(what, to)


@register.simple_tag
def concat_all(*args):
    """concatenate all args"""
    return ''.join(map(str, args))
