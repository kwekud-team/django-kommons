from dataclasses import dataclass


@dataclass
class LabelValue:
    label: str
    value: object
