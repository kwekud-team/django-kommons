from django.shortcuts import render
from django.views.generic import View


def static_request(request, template_name):
    """
        This is a short-cut so we can render template files, specifically static ones. We do this because we want to
        expose all template goodies like templatetags, filters and context variables.
    """
    content_type = None
    template_name = template_name.replace('__', '/')
    if '.' in template_name:
        file_type = template_name.split('.')[-1]
        mime_mapp = {'css': "text/css", 'js': "text/javascript"}
        content_type = mime_mapp.get(file_type)
    return render(request, template_name, {},  content_type=content_type)


class ReferrerRedirectView(View):
    referrer_key = None
    referrer_methods = []

    def dispatch(self, request, *args, **kwargs):
        self.save_referrer()
        return super().dispatch(request, *args, **kwargs)

    def get_referrer_key(self):
        key = self.referrer_key or self.__class__.__name__.lower()
        return f'referrer-key-{key}'

    def can_save(self):
        if self.referrer_methods == '__all__':
            return True
        else:
            return self.request.method.lower() in map(str.lower, self.referrer_methods)

    def save_referrer(self):
        referrer = self.request.META.get('HTTP_REFERER', None)
        # if referrer and self.request.method == 'GET':
        if referrer and self.can_save():
            key = self.get_referrer_key()
            self.request.session[key] = referrer

    def get_referrer(self):
        key = self.get_referrer_key()
        return self.request.session.get(key, None) or '/'
