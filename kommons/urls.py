from django.urls import path

from kommons import views

app_name = 'kommons'


urlpatterns = [
    path('static-request/<str:template_name>/', views.static_request, name="static_request"),
]
