from django.urls import reverse
from django.views.generic.base import TemplateView
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin

from kommons.utils.generic import intify


class BaseKProfileView(LoginRequiredMixin, TemplateView):
    current_user = None
    page_name = ''
    page_title = ''

    def get_context_data(self, **kwargs):
        context = super(BaseKProfileView, self).get_context_data(**kwargs)

        context.update({
            'current_user': self.current_user,
            'is_owner': self.check_is_owner(),
            'page_name': self.page_name,
            'page_title': self.page_title,
            'profile_actions': self.get_profile_actions(),
        })

        return context

    def get_current_user(self):
        """
            It is possible for user to enter this profile view without specifying
            user pk. Eg: when using social auth (google, facebook, ...)
            In that case, we just assume current user is logged in user.
        """

        pk = self.kwargs.get('pk', None)
        return (
            get_user_model().objects.filter(pk=intify(pk)).first()
            if pk
            else self.request.user
        )

    def check_is_owner(self):
        is_owner = False
        user = self.request.user

        if user.is_superuser:
            is_owner = True
        elif self.request.user.is_authenticated:
            is_owner = self.current_user == user

        return is_owner

    def get_profile_actions(self):
        return [
            # (reverse('account_profile'), 'overview', 'Overview'),
            (reverse('account_logout') + '?next=/', 'logout', 'Logout'),
        ]
