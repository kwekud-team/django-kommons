from django.test import TestCase
from django.contrib.sites.models import Site

from kommons.utils.http import build_site_uri


class KommonsHttpTests(TestCase):

    def test_build_site_uri(self):
        xs = [
            ('example.com', '', 'example.com/'),
            ('http://example.com', '', 'http://example.com/'),
            ('https://example.com', '', 'https://example.com/'),
            ('http://example.com', 'path', 'http://example.com/path/'),
            ('http://example.com', 'path/', 'http://example.com/path/'),
            ('http://example.com', '/path/', 'http://example.com/path/'),
            ('http://example.com', 'path/', 'http://example.com/path/'),
        ]

        for domain, path, expected_url in xs:
            site = Site.objects.get_or_create(domain=domain, name=domain)[0]
            site_url = build_site_uri(site, path)
            self.assertEqual(expected_url, site_url)
