import io
from PIL import Image
from dataclasses import dataclass
from django.test import TestCase
from django.forms import ValidationError

from kommons.utils.validators import image_resolution_validator


@dataclass
class ImageTestObj:
    _code: str
    _message: str

    result_type: object
    error_count: int
    # error_message: str

    width: int = 0
    height: int = 0

    def get_msg(self):
        return f'{self._code}:{self._message}'


class ImageResolutionValidatorsTests(TestCase):

    def _create_image(self, width, height):
        image_buffer = io.BytesIO()
        Image.new('RGB', (width, height)).save(image_buffer, "JPEG")
        return image_buffer

    def test_no_min_max(self):
        img = self._create_image(50, 50)
        res = image_resolution_validator(img)

        self.assertEqual(img, res)

    def test_validate_resolution(self):
        min_width, min_height = 50, 50
        max_width, max_height = 100, 100

        xs = [
            # min resolution
            ImageTestObj(_code='1.1', _message='width-under-height-under', width=40, height=40,
                         error_count=2, result_type=ValidationError),
            ImageTestObj(_code='1.2', _message='width-equal-height-under', width=50, height=40,
                         error_count=1, result_type=ValidationError),
            ImageTestObj(_code='1.3', _message='width-under-height-equal', width=40, height=50,
                         error_count=1, result_type=ValidationError),
            ImageTestObj(_code='1.4', _message='width-equal-height-equal', width=50, height=50,
                         error_count=0, result_type=io.BytesIO),
            ImageTestObj(_code='1.5', _message='width-over-height-over', width=60, height=60,
                         error_count=0, result_type=io.BytesIO),

            # max resolution
            ImageTestObj(_code='2.1', _message='width-over-height-over', width=110, height=110,
                         error_count=2, result_type=ValidationError),
            ImageTestObj(_code='2.2', _message='width-equal-height-over', width=100, height=110,
                         error_count=1, result_type=ValidationError),
            ImageTestObj(_code='2.3', _message='width-over-height-equal', width=110, height=100,
                         error_count=1, result_type=ValidationError),
            ImageTestObj(_code='2.4', _message='width-equal-height-equal', width=100, height=100,
                         error_count=0, result_type=io.BytesIO),
            ImageTestObj(_code='2.5', _message='width-under-height-under', width=90, height=90,
                         error_count=0, result_type=io.BytesIO),
        ]

        for test_obj in xs:
            img = self._create_image(test_obj.width, test_obj.height)
            res = image_resolution_validator(img, raise_error=False,
                                             min_res_width=min_width, min_res_height=min_height,
                                             max_res_width=max_width, max_res_height=max_height)
            if isinstance(res, ValidationError):
                self.assertEqual(1, len(res.error_list), msg=test_obj.get_msg())
            self.assertIsInstance(res, test_obj.result_type, msg=test_obj.get_msg())
