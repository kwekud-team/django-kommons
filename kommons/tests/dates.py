from django.test import TestCase
from django.utils import timezone

# from kommons.utils.dates import DateUtils


class KommonsDatesTests(TestCase):

    def test_date_tuples(self):
        limits = {
            'day': {'floor': -30},
            'week': {'floor': -30},
            'month': {'floor': -30},
            'year': {'floor': -30},
        }

        date_from = '2019-01-01'
        date_to = None
        period = 'day'
        # date_xs = DateUtils().get_date_tuples(date_from=date_from, date_to=date_to, period=period, limits=limits)

        # self.assertIs(date_xs[0][''], 'win')
        # self.assertIs(dt['first']['unique_state'], 'win')
        # self.assertIs(dt['first']['position'], 1)
        #
        # self.assertIs(dt['second']['regular_state'], 'na')
        # self.assertIs(dt['second']['unique_state'], 'na')
        # self.assertIs(dt['second']['position'], 2)
        #
        # self.assertIs(dt['third']['regular_state'], 'lose')
        # self.assertIs(dt['third']['unique_state'], 'lose')
        # self.assertIs(dt['third']['position'], 3)
