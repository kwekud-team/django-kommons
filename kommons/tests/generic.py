from django.test import TestCase
from decimal import Decimal
from kommons.utils.generic import dec_counter


class KommonsGenericTests(TestCase):

    def test_dec_counter(self):
        xs = [
            (0, (0, 0)),
            (0.0, (0, 0)),
            (0.1, (0, 1)),
            (2.1, (2, 1)),
            (None, (0, 0)),
            ('', (0, 0)),
            (Decimal('3.1'), (3, 1)),
            ('-1', (0, 0)),
            ('-1.1', (0, 1)),
            ('1.-1', (0, 0)),
            ('1234.1234', (1234, 1234)),
        ]

        for input_val, expected_counter in xs:
            res = dec_counter(input_val)
            self.assertEqual(expected_counter, res)
