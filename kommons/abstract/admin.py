from django.contrib import admin


class BaseAdmin(admin.ModelAdmin):
    show_all_qs = False
    filter_by_workspace = True

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
            obj.workspace = request.workspace

        obj.modified_by = request.user
        obj.save()
        return obj

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)

        for obj in formset.deleted_objects:
            obj.delete()

        for obj in instances:
            if not obj.date_created:
                obj.created_by = request.user
                obj.workspace = request.workspace

            obj.modified_by = request.user
            obj.save()
        formset.save_m2m()


class RequestFormBaseAdmin(admin.ModelAdmin):

    def get_form(self, request, obj=None, **kwargs):
        ModelForm = super(RequestFormBaseAdmin, self).get_form(request, obj, **kwargs)

        class ModelFormMetaClass(ModelForm):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return ModelForm(*args, **kwargs)

        return ModelFormMetaClass
