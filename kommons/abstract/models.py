import uuid
from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model


class BaseModel(models.Model):
    guid = models.UUIDField(blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_removed = models.DateTimeField(null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                   related_name='%(app_label)s%(class)s_created_by')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                    related_name='%(app_label)s%(class)s_modified_by')
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        super().save(*args, **kwargs)

    @staticmethod
    def get_system_user(username=None):
        system_username = username or 'system@example.com'

        user_model = get_user_model()
        user = user_model.objects.filter(username=system_username).first()
        return user or user_model.objects.create_user(system_username)
