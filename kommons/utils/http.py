import json
from urllib.parse import urlencode
from django.http import HttpResponseRedirect


def redirect_to_referer(request):
    referer_url = request.META.get('HTTP_REFERER', None) or '/'
    if '?next=' in referer_url:
        referer_url = referer_url.split('?next=')[1]

    return HttpResponseRedirect(referer_url)


class RequestsWrapper:

    def __init__(self, request_object):
        self.request_object = request_object

    def get_json(self):
        try:
            res = self.request_object.json()
        except json.decoder.JSONDecodeError:
            res = {}

        return res


def get_request_site(request):
    from django.contrib.sites.models import Site

    base_path = request.get_host()
    return Site.objects.filter(domain__iendswith=base_path).first()


def get_referrer_url(request):
    url = request.META.get('HTTP_REFERER', None) or '/'
    if '?next=' in url:
        url = url.split('?next=')[1]

    site_next = request.GET.get("site_next", None)
    if site_next:
        url = site_next

    return url


def attach_params(url, params):
    if params:
        if isinstance(params, dict):
            params = urlencode(params)

        sep = '&' if '?' in url else '?'
        return f'{url}{sep}{params}'

    return url


def get_session_key(request):
    # For anonymous users
    if not request.session or not request.session.session_key:
        request.session.save()

    session_key = request.session.session_key

    if not session_key:
        session_key = request.session._get_or_create_session_key()

    if not request.user.is_authenticated:
        # Only update special key if anonymous. This is to so logged in user can access same value.
        request.session['anon_key'] = session_key

    return session_key


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    return (
        x_forwarded_for.split(',')[-1].strip()
        if x_forwarded_for
        else request.META.get('REMOTE_ADDR')
    )


def build_site_uri(site, location=None):
    url = site.domain[:-1] if site.domain.endswith('/') else site.domain
    if location:
        path = location + '/' if not location.endswith('/') else location
        path = '/' + path if not path.startswith('/') else path
    else:
        path = '/'
    return f'{url}{path}'
