from kommons.utils.dates import parse_date


class RequestFilters:
    exclude_fields = ['page']

    def __init__(self, data, form=None):
        self.data = data
        self.form = form

    def gen_filters(self, valid_fields=[], exclude_fields=[], keep_fieldnames=False, djadmin_hack=False):
        filters = {}

        valid_fields = self.get_valid_fields(valid_fields)
        exclude_fields = self.get_exclude_fields(exclude_fields)

        vmap = {
            '__gte': {'value': ' 00:00'},
            '__lte': {'value': ' 23:59'},
            '_after': {'value': ' 00:00', 'lookup': '__gte'},
            '_before': {'value': ' 23:59', 'lookup': '__lte'},
        }
        for x in self.data.items():
            filtr = ''
            field, value = x
            # if request:
            vals = [v for v in self.data.getlist(field) if v]
            if len(vals) > 1:
                if not keep_fieldnames:
                    filtr = '__in'
                value = vals

            if field.endswith('__in'):  # and ',' in value:
                value = value.split(',')

            # Hack for date values
            for k, v in vmap.items():
                if field.endswith(k) and value:
                    value = '%s%s' % (value, v['value'])
                    value = parse_date(value)

                    if valid_fields:
                        lookup = v.get('lookup', k)
                        field = field.replace(k, lookup)
                        valid_fields.append(field)

            if (
                value
                and field not in exclude_fields
                and (valid_fields and field in valid_fields or not valid_fields)
            ):
                filters[str(field) + filtr] = value
        return filters

    def get_valid_fields(self, valid_fields):
        form_fields = valid_fields
        if not form_fields and self.form:
            form_fields = list(self.form.fields)

        return form_fields

    def get_exclude_fields(self, exclude_fields):
        return self.exclude_fields + exclude_fields

