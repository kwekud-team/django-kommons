import json
from decimal import Decimal
from django.template.defaultfilters import floatformat
from django.contrib.humanize.templatetags.humanize import intcomma

from kommons.utils.math import get_decimal_places


def intify(val, default=0):
    try:
        return int(val)
    except (ValueError, TypeError):
        return default


def floatify(val, default=0):
    try:
        return float(val)
    except:
        return default


def decify(val, default=None, decimal_places=None):
    try:
        val = Decimal(val)
        if decimal_places:
            val = get_decimal_places(val, decimal_places=decimal_places)
        return val
    except:
        return default or Decimal('0')


def boolify(val, default=False):
    bmap = {
        False: [0, '0', 'No', 'no', 'false', 'False', False],
        True: [1, '1', 'Yes', 'yes', 'true', 'True', True],
    }
    for key, vals in bmap.items():
        if val in vals:
            return key

    return default


def jsonify(value, default=None):
    val = default or {}
    if value:
        try:
            val = json.loads(value)
        except json.JSONDecodeError:
            pass

    return val


def get_sets(total_cnt, limit=300):
    divs = intify(total_cnt / limit)
    page_sets = [
        (x * limit, (x + 1) * limit, '%s:%s' % (x * limit, (x + 1) * limit))
        for x in range(divs)
    ]

    page_sets.append((divs * limit, total_cnt, '%s:%s' % (divs * limit, total_cnt)))

    return page_sets


def get_recursive_attr(obj, attrs, default=None):
    for x in attrs:
        try:
            obj = getattr(obj, x, default)()  # for functions
        except:
            obj = getattr(obj, x, default)
    return obj


def get_dotted_dict(dict_obj, keys, default=None, separator='.', handle_case_insensitive_keys=True):
    val = dict_obj
    for x in keys.split(separator):
        if isinstance(val, dict):
            if handle_case_insensitive_keys:
                val_lower = {k.lower(): v for k, v in val.items()}
                val = val_lower.get(str(x).lower())
            else:
                val = val.get(str(x))
        elif type(val) in (list, tuple):
            try:
                val = val[intify(x)]
            except IndexError:
                val = None

    return val if val is not None else default


def update_dotted_dict(dict_obj, keys, value, separator='.'):
    split_keys = keys.split(separator)

    old_key = split_keys[-1]
    val = {old_key: value}

    for x in sorted(split_keys[:-1], reverse=True):
        val[x] = val.copy()
        del val[old_key]
        old_key = x

    dict_obj.update(val)
    return dict_obj


def monefy(val, decimal_places=2):
    return intcomma(floatformat(val, decimal_places))


def convert_base(number, base, alphabets="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"):
    res = ""
    number = intify(number)
    while number:
        res += alphabets[number % base]
        number //= base
    return res[::-1] or "0"


def dec_counter(value):
    """
        Similar to semvar, but only in 2 parts. This is useful for implementing counters that reset after a threshold.
        Eg: 0.1, 0.2, 0.3 -> 1.1, 1.2, 1.3

        :return tuple with (major, minor)
    """

    # TODO: optimize and cleanup code
    dec_value = decify(str(value))
    major = int(dec_value)
    minor = int(str(dec_value).split('.')[1]) if '.' in str(dec_value) else 0

    return max(major, 0), max(minor, 0)


def mask_ending_text(text, mask_character='*', unmask_length=3):
    unmasked = str(text)
    unmask_position = unmask_length * -1
    return len(unmasked[:unmask_position]) * mask_character + unmasked[unmask_position:]

