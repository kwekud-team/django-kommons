from decimal import Decimal


def get_percentage_change(old_value, new_value):
    per_change = 0
    if old_value:
        diff = new_value - old_value
        per_change = (Decimal(diff) / Decimal(old_value)) * 100
    return get_decimal_places(per_change)


def get_decimal_places(decimal_value, decimal_places=2):
    dec_zeroes = (decimal_places - 1) * '0'
    return Decimal(decimal_value).quantize(Decimal("0.%s1" % dec_zeroes))


def base36encode(number):
    if not isinstance(number, (int,)):
        raise TypeError('number must be an integer')
    if number < 0:
        raise ValueError('number must be positive')

    alphabet, base36 = ['0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', '']

    while number:
        number, i = divmod(number, 36)
        base36 = alphabet[i] + base36

    return base36 or alphabet[0]


def base36decode(number):
    return int(number, 36)
