from collections import defaultdict


class Stack:

    def __init__(self, ):
        self.stack = []

    def push(self, value):
        self.stack.append(value)

    def pop(self):
        if self.stack:
            self.stack.pop()

    def peak(self):
        if self.stack:
            return self.stack[-1]

    def size(self):
        return len(self.stack)


class Queue:
    def __init__(self):
        self.queue_list = []

    def __str__(self): 
        return ' '.join([str(i) for i in self.queue_list])

    def enqueue(self, value):
        self.queue_list.insert(0, value)

    def dequeue(self):
        if self.queue_list:
            return self.queue_list.pop()

    def size(self):
        return len(self.queue_list)

    def is_empty(self):
        return self.size() <= 0


class PriorityMaxQueue(Queue):  
    def dequeue(self): 
        try: 
            max_val = 0
            for i in range(len(self.queue_list)):
                if self.queue_list[i] > self.queue_list[max_val]:
                    max_val = i 
            item = self.queue_list[max_val]
            del self.queue_list[max_val]
            return item 
        except IndexError: 
            return None


class PriorityMinQueue(Queue):  
    def dequeue(self): 
        try: 
            min_val = 0
            for i in range(len(self.queue_list)):
                if self.queue_list[i] < self.queue_list[min_val]:
                    min_val = i 
            item = self.queue_list[min_val]
            del self.queue_list[min_val]
            return item 
        except IndexError: 
            return None


class LinkedList:

    def __init__(self, val):
        self.val = val
        self.next = None  # the pointer initially points to nothing

    def traverse(self):
        node = self
        while node is not None:
            node = node.next


class LeftRightNode:

    def __init__(self, value):
        self.value = value
        self.child_left = None
        self.child_right = None 


class MultiChildNode:

    def __init__(self, value):
        self.value = value
        self.children = []

    def add_children(self, *values):
        self.children.extend(values)


class Graph:

    def __init__(self):
        self.graph = defaultdict(list)

    def add_edge(self, pos, value): 
        self.graph[pos].append(value)

    def get_edge(self, pos):
        return self.graph[pos]


class PriorityTreeQueue:
    def __init__(self):
        self.queue_list = [0]
        self.current_size = 0

    def enqueue(self, value):
        self.queue_list.append(value)
        # self.current_size += 1
        self.current_size = self.current_size + 1
        self.fix_up(self.current_size)

    def fix_up(self, index):
        while index // 2 > 0:
            # if self.heapList[i] < self.heapList[i // 2]:
            if self.queue_list[index] < self.queue_list[index // 2]:
                temp = self.queue_list[index // 2]
                self.queue_list[index // 2] = self.queue_list[index]
                self.queue_list[index] = temp

            # Set index to parent index to go up parent and also fix up
            index = index // 2

    def fix_down(self, index): 
        while index * 2 <= self.current_size:
            mc = self.min_child(index)
            if self.queue_list[index] > self.queue_list[mc]:
                tmp = self.queue_list[index]
                self.queue_list[index] = self.queue_list[mc]
                self.queue_list[mc] = tmp
            index = mc

    def min_child(self, i):
        if i * 2 + 1 > self.current_size:
            return i * 2
        if self.queue_list[i*2] < self.queue_list[i*2+1]:
            return i * 2
        else:
            return i * 2 + 1

    def dequeue(self):
        retval = self.queue_list[1]
        self.queue_list[1] = self.queue_list[self.current_size]
        self.current_size = self.current_size - 1
        self.queue_list.pop()
        self.fix_down(1)
        return retval

    def build_heap(self, elements):
        i = len(elements) // 2
        self.current_size = len(elements)
        self.queue_list = [0] + elements[:]
        while i > 0:
            self.fix_down(i)
            i = i - 1
