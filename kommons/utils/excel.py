import os
import xlwt
import tablib

from django.conf import settings
from django.http import HttpResponse
from django.utils import timezone
from django.template.defaultfilters import slugify
from kommons.utils.files import mkdir_p


class ExcelExporter:

    def __init__(self, filename='export', fformat='xls', append_filename_timestamp=True):
        self.filename = filename
        self.fformat = fformat
        self.append_filename_timestamp = append_filename_timestamp
        self.workbook = self.get_workbook()

    def get_workbook(self):
        raise NotImplemented

    def get_filename(self):
        filename = self.filename
        if self.append_filename_timestamp:
            filename = '%s-%s' % (filename, timezone.now().strftime("%Y%m%d%H%M%S"))
        return '%s.%s' % (slugify(filename), self.fformat)


class TablibExcelExporterWT(ExcelExporter):

    def get_workbook(self):
        return

    def build_dataset(self, sheets):
        book = tablib.Databook(sheets)
        book.export(self.fformat)
        return book

    @staticmethod
    def build_sheet(info):
        data = tablib.Dataset()
        data.title = info['title']
        data.headers = [x.upper() for x in info['headers']]
        data.extend(info['data'])
        return data

    def export(self, dataset):
        content_type = 'application/%s' % self.fformat
        full_filename = self.get_filename()

        response = HttpResponse(getattr(dataset, self.fformat), content_type=content_type)
        response['Content-Disposition'] = 'attachment; filename=%s' % full_filename
        return response


class XlwtExcelExporter(ExcelExporter):

    def get_workbook(self):
        return xlwt.Workbook()

    def build_dataset(self, sheets):
        return self.workbook

    def build_sheet(self, info):
        sheet = self.workbook.add_sheet(info['title'])
        self.build_headers(sheet, info['headers'])
        self.build_body(sheet, len(info['headers']), info['body_matrix'])
        return sheet

    def build_headers(self, sheet, headers):
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        alignment = xlwt.Alignment()
        alignment.horz = xlwt.Alignment.HORZ_CENTER
        font_style.alignment = alignment

        skip_col_width = []
        for row_num, row in enumerate(headers):
            adj_cnt = 0
            for col_num, col_val in enumerate(row):
                col_num += adj_cnt
                if type(col_val) == list:
                    comb_val = col_val[0].upper()
                    sheet.write_merge(row_num, row_num, col_num, col_num + len(col_val) - 1, comb_val, font_style)
                    width = arial10.fitwidth(comb_val)
                    sheet.col(col_num).width = int(width) + 3000
                    # skip_col_width.extend( [x for x in range(col_num, col_num + len(col_val))])
                    adj_cnt += len(col_val) - 1
                else:
                    sheet.write(row_num, col_num, col_val, font_style)
                    if col_num not in skip_col_width:
                        width = arial10.fitwidth(col_val)
                        sheet.col(col_num).width = int(width) + 3000

    def build_body(self, sheet, start_row, matrix_body):
        font_style = xlwt.XFStyle()

        for row_num in range(matrix_body.row_count):
            for col_num in range(matrix_body.col_count):
                cell_val = matrix_body.get_record(row_num, col_num)
                sheet.write(start_row + row_num, col_num, cell_val, font_style)

    def export_to_response(self, workbook):
        full_filename = self.get_filename()
        content_type = 'application/%s' % self.fformat

        response = HttpResponse(content_type=content_type)
        response['Content-Disposition'] = 'attachment; filename=%s' % full_filename
        workbook.save(response)
        return response

    def export_to_file(self, workbook, folder_path):
        full_filename = self.get_filename()

        save_folder = os.path.join(settings.MEDIA_ROOT, folder_path)
        mkdir_p(save_folder)

        path = os.path.join(save_folder, full_filename)
        workbook.save(path)

        url = '%s%s/%s' % (settings.MEDIA_URL, folder_path, full_filename)

        # print("SAVED TO: %s" % path)
        return {
            'is_valid': True,
            'path': path,
            'url': url
        }

