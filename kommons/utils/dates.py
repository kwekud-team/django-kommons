from django.conf import settings
from django.utils import timezone
from django.template.defaultfilters import date, time
from dateutil import rrule as dt_rrule
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse

from kommons.constants import KommonsK


def parse_date(date_str, make_aware=True):
    try:
        date_obj = parse(date_str)
    except (ValueError, AttributeError, TypeError):
        date_obj = None

    if date_obj and make_aware:
        try:
            date_obj = timezone.make_aware(date_obj, timezone.get_default_timezone())
        except ValueError:
            pass

    return date_obj


def get_relative_date(period_type, period_value):
    valid_periods = [
        KommonsK.DATES_PERIOD_SECONDS.value, KommonsK.DATES_PERIOD_MINUTES.value, KommonsK.DATES_PERIOD_HOURS.value,
        KommonsK.DATES_PERIOD_DAYS.value, KommonsK.DATES_PERIOD_WEEKS.value, KommonsK.DATES_PERIOD_MONTHS.value,
        KommonsK.DATES_PERIOD_YEARS.value,
    ]
    if period_type in valid_periods:
        return relativedelta(**{period_type: period_value})


def get_rules(date_from, date_to):
    return {
        KommonsK.DATES_PERIOD_DAYS.value: {
            'rule': dt_rrule.DAILY, 'short_format': '%Y-%m-%d', 'long_format': '%a %d %b, %Y', 'extra_': {'count': 1}
        },
        KommonsK.DATES_PERIOD_WEEKS.value: {
            'rule': dt_rrule.WEEKLY, 'short_format': '%Y-%m-%d', 'long_format': '%Y, %V',
            'extra': {'byweekday': date_to.weekday()}
        },
        KommonsK.DATES_PERIOD_MONTHS.value: {
            'rule': dt_rrule.MONTHLY, 'short_format': '%b %Y', 'long_format': '%b %Y',
            'extra': {'bymonthday': date_to.day}
        },
        # 'year': {'rule': YEARLY, 'short_format': '%Y', 'long_format': '%Y', 'extra': {'byyearday': date_to.day}},
        KommonsK.DATES_PERIOD_YEARS.value: {
            'rule': dt_rrule.YEARLY, 'short_format': '%Y', 'long_format': '%Y', 'extra': {'bysetpos': -1}
        },
    }


def get_date_range(date_from=None, date_to=None, period=KommonsK.DATES_PERIOD_DAYS.value, from_interval=7,
                   to_interval=1):
    today = timezone.now()
    period = period or KommonsK.DATES_PERIOD_DAYS.value
    # period += 's'

    if type(date_from) == str:
        date_from = parse_date(date_from)
    if not date_from:
        date_from = (today - relativedelta(**{period: from_interval}))

    if type(date_to) == str:
        date_to = parse_date(date_to)
    if not date_to:
        from_dt = today
        date_to = (from_dt + relativedelta(**{period: to_interval}))

    return date_from, date_to


def get_period_dates(date_from=None, date_to=None, period=KommonsK.DATES_PERIOD_DAYS.value, limits=None,
                     from_interval=7, to_interval=1):
    limits = limits or {}
    date_from, date_to = get_date_range(date_from, date_to, period=period, from_interval=from_interval,
                                        to_interval=to_interval)
    rule_map = get_rules(date_from, date_to)
    obj = rule_map.get(period, None) or rule_map[KommonsK.DATES_PERIOD_DAYS.value]

    extra = obj.get('extra', {})
    dates = list(dt_rrule.rrule(obj['rule'], dtstart=date_from, until=date_to, **extra))

    if len(dates) > 1:
        floor = limits.get(period, {}).get('floor', None)
        ceiling = limits.get(period, {}).get('ceiling', None)
        limited_dates = list(dates[floor:ceiling])
    else:
        limited_dates = [date_from, date_to]

    date_ranges = []
    for pos, x in enumerate(limited_dates):
        if pos != len(dates) - 1:
            try:
                if period == KommonsK.DATES_PERIOD_DAYS.value:
                    to_date = limited_dates[pos]
                else:
                    to_date = limited_dates[pos + 1]
            except IndexError:
                to_date = limited_dates[pos]

            # to_date = limited_dates[pos]
            date_ranges.append({
                'from': parse_date(limited_dates[pos].strftime("%Y-%m-%d") + ' 00:00'),
                'to': parse_date(to_date.strftime("%Y-%m-%d") + ' 23:59'),
                'short_label': to_date.strftime(obj['short_format']),
                'long_label': to_date.strftime(obj['long_format'])
            })

    return date_ranges


intervals = (
    ('weeks', 604800),  # 60 * 60 * 24 * 7
    ('days', 86400),    # 60 * 60 * 24
    ('hours', 3600),    # 60 * 60
    ('minutes', 60),
    ('seconds', 1),
    )


def humanize_time(seconds, granularity=2):
    result = []

    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            if value == 1:
                name = name.rstrip('s')
            result.append("{} {}".format(value, name))
    return ', '.join(result[:granularity])


def display_date_time(date_obj):
    return f'{date(date_obj)} {time(date_obj)}'


def format_date_time(value, arg=None):
    """ Formats a datetime according to the given format.
        Django only has a function for date
    """
    from django.utils.dateformat import format
    if not value:
        return u''
    if arg is None:
        arg = settings.DATETIME_FORMAT
    try:
        return format(value, arg)
    except AttributeError:
        return ''