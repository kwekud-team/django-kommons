import phonenumbers
import logging
from django.core.files.images import get_image_dimensions
from django.forms import ValidationError
from kommons.utils.files import ByteConverter
from kommons.utils.generic import intify


# 4 * 1024 * 1024
def file_size_validator(file_object, min_size=0, max_size=0, readable_unit='mb'):

    def get_readable(size):
        if readable_unit == 'kb':
            return ByteConverter().byte_2_kb(size)
        else:
            return ByteConverter().byte_2_mb(size)

    if file_object:
        if max_size:
            if file_object.size > max_size:
                readable = get_readable(max_size)
                raise ValidationError(f'File too large; must be less than {readable}')
        if min_size:
            if file_object.size > min_size:
                readable = get_readable(min_size)
                raise ValidationError(f'File too small; must be greater than {readable}')

        return file_object
    else:
        return None


def image_resolution_validator(image_object, min_res_width=0, min_res_height=0, max_res_width=0, max_res_height=0,
                               field_label=None, raise_error=True):
    field_label = field_label or 'Image'

    # Force resolution params to int
    min_res_width, min_res_height = intify(min_res_width), intify(min_res_height)
    max_res_width, max_res_height = intify(max_res_width), intify(max_res_height)

    image_width, image_height = get_image_dimensions(image_object)

    errors = []
    if min_res_width and image_width < min_res_width:
        errors.append(f'width cannot be less than {min_res_width}')

    if min_res_height and image_height < min_res_height:
        errors.append(f'height cannot be less than {min_res_height}')

    if max_res_width and image_width > max_res_width:
        errors.append(f'width cannot be more than {max_res_width}')

    if max_res_height and image_height > max_res_height:
        errors.append(f'height cannot be more than {max_res_height}')

    if errors:
        prefix = f'Size of uploaded {field_label} is {image_width}x{image_height}.'
        message = ' and '.join(errors)
        validation_error = ValidationError(f'{prefix} {message.capitalize()}.')

        if raise_error:
            raise validation_error
        else:
            return validation_error

    return image_object


# TODO: Try and use PhoneValidator below.
def phone_validator(value, message=None):
    prep_value = value if value.startswith('+') else f'+{value}'
    try:
        phonenumbers.parse(prep_value)
    except phonenumbers.NumberParseException:
        message = message or "Please enter a correct phone number with the country code. eg: 233201234567"
        raise ValidationError(message)

    return value


class ValidatorException(Exception):
    """
    Create a generic exception that can be used with any library like, django.forms.ValidationError or
    rest_framework.serializers.ValidationError
    """
    def __init__(self, message: str, code: str = None, *args, **kwargs):
        self.message = message
        self.code = code
        super().__init__(message, code)


class BaseValidator:
    def __init__(self, message: str = None, code: str = None):
        self.message = message
        self.code = code


class PhoneValidator(BaseValidator):

    def __init__(self, message=None, code=None):
        super().__init__(message, code)

    def __call__(self, value):
        if not is_valid_phone(value):
            message = self.message or "Please enter a correct phone number with the country code. eg: 233201234567"
            raise ValidatorException(message, code='invalid-number')


def is_valid_phone(value):
    # Append '+' if value does not start with it
    prep_value = value if value.startswith('+') else f'+{value}'

    try:
        parsed_num = phonenumbers.parse(prep_value)
        return phonenumbers.is_valid_number(parsed_num)
    except Exception as e:
        logging.debug(str(e))
        return False
