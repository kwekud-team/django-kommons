
def dotted_import(dotted_path):
    if dotted_path:
        obj = dotted_path.split('.')[-1]
        path = '.'.join(dotted_path.split('.')[:-1]) 
        mod = __import__(path, fromlist=[obj])
        return getattr(mod, obj, None)


def get_class_path(obj):
    module = obj.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return obj.__class__.__name__
    return module + '.' + obj.__class__.__name__
