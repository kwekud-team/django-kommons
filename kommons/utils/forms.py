

def get_choice_tuple_from_list(list_of_items, empty_option=True):
    xs = [(x, str(x).title()) for x in list_of_items]
    if empty_option:
        xs = [('', '---')] + xs
    return tuple(xs)
