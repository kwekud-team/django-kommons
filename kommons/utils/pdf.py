import base64
import cgi
import logging
import os
from io import BytesIO
from urllib.request import urlopen

from PIL import Image as PILImage
from PIL import ImageEnhance
from django.conf import settings
from django.http import HttpResponse
from django.template.defaultfilters import slugify
from django.template.loader import get_template
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.pagesizes import landscape, portrait
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, Image
from reportlab.platypus.flowables import Spacer
import xhtml2pdf.pisa as pisa


class MySimpleDocTemplate(SimpleDocTemplate):
    orientation = None
    background_img_path = None
    start_canvas_callback = None
    end_canvas_callback = None

    def _startBuild(self, filename=None, canvasmaker=canvas.Canvas):
        super()._startBuild(filename=filename, canvasmaker=canvasmaker)

        if self.start_canvas_callback:
            self.start_canvas_callback(self.canv)

    def _endBuild(self):
        page_size = self.canv._pagesize
        if self.orientation == 'portrait':
            self.canv.setPageSize(portrait(page_size))

        elif self.orientation == 'landscape':
            self.canv.setPageSize(landscape(page_size))

        if self.end_canvas_callback:
            self.end_canvas_callback(self.canv)

        super(MySimpleDocTemplate, self)._endBuild()


def page_info(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman', 9)
    canvas.drawString(cm, 0.5 * cm, "Page %s" % doc.page)

    if doc.background_img_path:
        canvas.restoreState()
        # canvas.drawImage(doc.background_img_path, 0, 0, 21 * cm, 29.7 * cm)
        canvas.drawImage(doc.background_img_path, 0, 0, *doc.pagesize)
        # ore/website/static/website/local/img/avatar.png", 0, 0, 4.92 * inch, 3.46 * inch)


def create_pdfdoc(pdfdoc, story, page_size=A4, margin_size=cm, frames=None, show_page_number=False, rotation=0,
                  orientation=None, use_dynamic_heights=False, background_img_path=None, end_canvas_callback=None,
                  start_canvas_callback=None):
    """
    Creates PDF doc from story.
    """
    frames = frames or []
    ms = margin_size[:4]

    if len(ms) == 1:
        tm, rm, bm, lm = ms[0], ms[0], ms[0], ms[0]
    elif len(ms) == 2:
        tm, rm, bm, lm = ms[0], ms[1], ms[0], ms[1]
    elif len(ms) == 3:
        tm, rm, bm, lm = ms[0], ms[1], ms[2], ms[1]
    else:
        tm, rm, bm, lm = ms

    if use_dynamic_heights:
        actual_width, actual_height = page_size

        th = 0
        for x in story:
            w, h = x.wrap(actual_width - lm, actual_height)
            th += h
        th += 50

        page_size = (actual_width, th)

    pdf_doc = MySimpleDocTemplate(pdfdoc, frames=frames, pagesize=page_size, leftMargin=lm, rightMargin=rm,
                                  topMargin=tm, bottomMargin=bm, rotation=rotation, start_canvas_callback=start_canvas_callback,
                                  end_canvas_callback=end_canvas_callback)
    pdf_doc.orientation = orientation
    pdf_doc.start_canvas_callback = start_canvas_callback
    pdf_doc.end_canvas_callback = end_canvas_callback
    pdf_doc.background_img_path = background_img_path

    kwargs = {}

    if show_page_number or background_img_path:
        kwargs.update({'onFirstPage': page_info, 'onLaterPages': page_info})

    pdf_doc.build(story, **kwargs)


def pdf_story_response(story, **kwargs):
    page_size = kwargs.pop('page_size', A4)
    margin_size = kwargs.pop('margin_size', cm)
    file_name = kwargs.pop('file_name', None)
    should_download = kwargs.pop('should_download', False)
    show_page_number = kwargs.pop('show_page_number', False)
    use_dynamic_heights = kwargs.pop('use_dynamic_heights', False)
    rotation = kwargs.pop('rotation', 0)
    background_img_path = kwargs.pop('background_img_path', None)
    orientation = kwargs.pop('orientation', None)
    frames = kwargs.pop('frames', [])
    start_canvas_callback = kwargs.pop('start_canvas_callback', None)
    end_canvas_callback = kwargs.pop('end_canvas_callback', None)
    render_to = kwargs.pop('render_to', None)

    pdf_buffer = BytesIO()

    create_pdfdoc(pdf_buffer,
                  story,
                  frames=frames,
                  rotation=rotation,
                  orientation=orientation,
                  page_size=page_size,
                  margin_size=margin_size,
                  show_page_number=show_page_number,
                  use_dynamic_heights=use_dynamic_heights,
                  background_img_path=background_img_path,
                  start_canvas_callback=start_canvas_callback,
                  end_canvas_callback=end_canvas_callback,
                  )

    if render_to == 'png':
        return _render_to_png(pdf_buffer)
    else:
        return _render_to_pdf(pdf_buffer, should_download, file_name)


def _render_to_pdf(pdf_buffer, should_download, file_name):
    pdf = pdf_buffer.getvalue()
    response = HttpResponse(content_type='application/pdf')
    render_type = 'attachment' if should_download else 'inline'
    if file_name:
        response['Content-Disposition'] = '%s; filename="%s.pdf"' % (render_type, slugify(file_name))

    pdf_buffer.close()
    response.write(pdf)

    return response

def _render_to_png(pdf_buffer):
    from pdf2image import convert_from_bytes

    pdf_buffer.seek(0)
    image = convert_from_bytes(pdf_buffer.getvalue(), fmt='png')[0]

    # Convert to PNG
    png_buffer = BytesIO()
    image.save(png_buffer, format="PNG")

    png_buffer.seek(0)
    return HttpResponse(png_buffer, content_type='image/png')


class BasePdfView(object):
    title = 'PDF'
    page_size = A4
    margin_size = [1 * cm]

    def __init__(self):
        self.default_style = getSampleStyleSheet()["BodyText"]

    def get_page_size(self):
        return self.page_size

    def get_margin_size(self):
        return self.margin_size

    def get_filename(self):
        return self.title

    def build_header_story(self):
        styles = getSampleStyleSheet()
        body_style = styles["BodyText"]
        story = []

        # Page header
        page_title = Paragraph(
            """
                <para>
                    <font size="20"><b>Midland Health</b></font>
                    <br /> On site, Everywhere.
                </para>
            """,
            body_style)

        img_path = ''
        if os.path.exists(img_path):
            f = open(img_path, 'rb')
            img = Image(f, width=2 * cm, height=2 * cm, kind='proportional')
        else:
            img = ""

        header_table_data = ([img, page_title],)

        table = Table(header_table_data,
                      colWidths=(3 * cm, '*'),
                      rowHeights=[1.5 * cm],
                      style=[
                          ('LINEBELOW', (0, -1), (-1, -1), 2, colors.grey),
                          ('VALIGN', (0, -1), (1, -1), 'TOP'),
                      ])
        story.extend([table, Spacer(0, 0.4 * cm)])

        # Report title
        title = Paragraph("<para align=center size=16><b><u>%s</u></b></para>" % self.title, body_style)
        story.extend([title, Spacer(0, 0.4 * cm)])

        return story

    def image_from_path(self, img_path, width=2 * cm, height=2 * cm):
        img = ''
        if os.path.exists(img_path):
            f = open(img_path, 'rb')
            try:
                img = Image(f, width=width, height=height, kind='proportional')
            except Exception as e:
                logging.warning(str(e))
        return img

    def get_image_from_base64(self, data, width=2 * cm, height=2 * cm, kind='proportional', brighten_factor=0):
        img = ''
        try:
            byte_data = BytesIO(base64.b64decode(data))

            if brighten_factor:
                byte_data = self._brighten_image(byte_data, brighten_factor)

            img = Image(byte_data, width=width, height=height, kind=kind)
        except Exception as e:
            logging.warning(str(e))
        return img

    def get_image_from_url(self, image_url, width=2 * cm, height=2 * cm):
        img = ''
        try:
            response = urlopen(image_url).read()
            b64 = base64.b64encode(response)
            img = self.get_image_from_base64(b64, width=width, height=height)
        except Exception as e:
            logging.warning(f'Error loading image url: {image_url}. {e}')
        return img

    @staticmethod
    def _brighten_image(byte_data, factor):
        pil_img = PILImage.open(byte_data)
        fformat = pil_img.format
        pil_img = ImageEnhance.Brightness(pil_img).enhance(factor)

        img_io = BytesIO()
        pil_img.save(img_io, fformat)

        return img_io


def get_pisa_doc(template_src, context_dict):
    result = BytesIO()

    # find the template and render it.
    template = get_template(template_src)
    html = template.render(context_dict)

    # create a pdf
    pisaStatus = pisa.CreatePDF(
       html, dest=result, link_callback=link_callback, debug=1)

    return {
        'html': html,
        'result_io': result,
        'error': pisaStatus.err,
    }


def link_callback(uri, rel):
    if uri.startswith(settings.MEDIA_URL):
        path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
    elif uri.startswith(settings.STATIC_URL):
        path = os.path.join(settings.STATIC_ROOT, uri.replace(settings.STATIC_URL, ""))
    else:
        path = os.path.join(settings.STATIC_ROOT, uri.replace(settings.STATIC_URL, ""))

        if not os.path.isfile(path):
            path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))

            if not os.path.isfile(path):
                raise Exception('media urls must start with %s or %s' % (settings.MEDIA_ROOT, settings.STATIC_ROOT))
    return path


def render_to_pdf(template_src, context_dict, request=None):
    doc = get_pisa_doc(template_src, context_dict)

    if doc['error']:
        response = HttpResponse('We had some errors<pre>%s</pre>' % cgi.escape(doc['html']))
    else:
        # response = HttpResponse(content_type='application/pdf')
        response = HttpResponse(doc['result_io'].getvalue(), content_type='application/pdf;')
        response['Content-Disposition'] = 'inline; filename="report.pdf"'

        # find the template and render it.
        template = get_template(template_src)
        html = template.render(context_dict)

        # create a pdf
        pisaStatus = pisa.CreatePDF(
           html, dest=response, link_callback=link_callback, debug=1)

    return response


class PdfImageUtils:

    @staticmethod
    def image_from_path(img_path, width=2 * cm, height=2 * cm):
        img = ''
        if os.path.exists(img_path):
            f = open(img_path, 'rb')
            try:
                img = Image(f, width=width, height=height, kind='proportional')
            except Exception as e:
                logging.error(str(e))
        return img

    def get_image_from_base64(self, data, width=2 * cm, height=2 * cm, kind='proportional', brighten_factor=0, background_color=None):
        byte_data = self._get_base64_to_bytes(data, brighten_factor=brighten_factor, background_color=background_color)
        image = (
            Image(byte_data, width=width, height=height, kind=kind)
            if byte_data
            else ''
        )
        return image

    def get_imagereader_from_base64(self, data, brighten_factor=0, background_color=None):
        byte_data = self._get_base64_to_bytes(data, brighten_factor=brighten_factor, background_color=background_color)
        return (
            ImageReader(byte_data)
            if byte_data
            else ''
        )

    def _get_base64_to_bytes(self, data, brighten_factor=0, background_color=None):
        byte_data = None
        try:
            byte_data = BytesIO(base64.b64decode(data))
            if background_color:
                byte_data = self._set_background_color(byte_data, background_color)

            if brighten_factor:
                byte_data = self._brighten_image(byte_data, brighten_factor)

        except Exception as e:
            logging.error(str(e))
        return byte_data

    @staticmethod
    def _brighten_image(byte_data, factor):
        pil_img = PILImage.open(byte_data)
        fformat = pil_img.format
        pil_img = ImageEnhance.Brightness(pil_img).enhance(factor)

        img_io = BytesIO()
        pil_img.save(img_io, fformat)

        return img_io

    @staticmethod
    def _set_background_color(byte_data, background_color=None):
        pil_img = PILImage.open(byte_data)
        fformat = pil_img.format

        background_color = background_color or (255, 255, 255)
        new_image = PILImage.new('RGB', pil_img.size, background_color)
        new_image.paste(pil_img, (0, 0), pil_img)

        img_io = BytesIO()
        new_image.save(img_io, fformat)

        return img_io

    @staticmethod
    def get_image_from_field(image, width=50, height=50, kind='proportional'):
        if image:
            img = BytesIO(image.file.read())
            return Image(img, width=width, height=height, kind=kind)
        return ""