import errno
import os
from os.path import isfile, join, isdir

from kommons.utils.generic import floatify


def mkdir_p(path):
    if not os.path.exists(path): 
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno != errno.EEXIST or not os.path.isdir(path):
                raise
    return path


def get_files_in_dir(directory_path):
    return [f for f in os.listdir(directory_path) if isfile(join(directory_path, f))]


def get_sub_directories(directory_path):
    return [f for f in os.listdir(directory_path) if isdir(join(directory_path, f))]


def is_file_exists(file_path):
    return os.path.isfile(file_path)


class ByteConverter:
    B = 1
    KB = float(1024)
    MB = float(KB ** 2)  # 1,048,576
    GB = float(KB ** 3)  # 1,073,741,824
    TB = float(KB ** 4)  # 1,099,511,627,776

    def mb_2_bytes(self, value):
        return floatify(value) * self.MB

    def byte_2_kb(self, value):
        return self.pretify(floatify(value) / self.KB, 'KB')

    def byte_2_mb(self, value):
        return self.pretify(floatify(value) / self.MB, 'MB')

    def byte_2_gb(self, value):
        return self.pretify(floatify(value) / self.GB, 'GB')

    def pretify(self, converted_value, unit):
        return '{0:.2f} {1}'.format(converted_value, unit)