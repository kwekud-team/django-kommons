from django.apps import apps
from django.core.exceptions import FieldDoesNotExist
from django.db.models.fields.related_descriptors import ForwardManyToOneDescriptor, ReverseOneToOneDescriptor


def get_str_from_model(model):
    """ Get model from string in format 'app_label.model_name' """
    if isinstance(model, ReverseOneToOneDescriptor):
        model = model.related.related_model
    elif isinstance(model, ForwardManyToOneDescriptor):
        model = model.field.related_model

    app_label = model._meta.app_label
    model_name = model._meta.model_name
    return '%s.%s' % (app_label, model_name)


def get_model_from_str(app_model, raise_exception=False):
    """ Get model from string in format 'app_label.model_name' """
    app_model = str(app_model)
    if app_model and '.' in app_model:
        app_label, model_name = app_model.split('.')
        try:
            mc = apps.get_model(app_label, model_name)
        except LookupError:
            mc = None
        if not mc and raise_exception:
            raise Exception("App model not found: %s" % app_model)
        return mc


def get_object_from_pk(app_model, pk):
    model_class = get_model_from_str(app_model)
    return model_class.objects.filter(pk=pk).first() if model_class else None


def get_position_in_queryset(queryset, obj):
    qs_pks = queryset.values_list('pk', flat=True)
    try:
        pos = list(qs_pks).index(obj.pk) + 1
    except ValueError:
        pos = -1

    return pos


def get_recursive_field(field_name, model_class=None, obj=None, splitter='__', exclude=None):
    exclude = exclude or []
    levels = field_name.split(splitter)
    model_class = (obj._meta.concrete_model if obj else model_class)
    values = {
        'old_field_name': field_name,
        'old_obj': obj,
        'old_model_class': model_class
    }
    field = None
    for pos, x in enumerate(levels):
        if x not in exclude:
            field_name = x
            try:
                field = model_class._meta.get_field(x)
                field_name = field.name
                if getattr(field, 'rel', None):
                    model_class = field.rel.to
            except FieldDoesNotExist:
                field = None

            if obj and not pos == len(levels) - 1:
                if obj.pk:
                    obj = getattr(obj, x, None)
    values.update({
        'field_name': field_name,
        'model_class': model_class,
        'obj': obj,
        'field': field
    })

    return values

def check_fields_exists(model_or_instance, fields: list):
    dt = {'exists': [], 'absent': []}
    for field in fields:
        try:
            model_or_instance._meta.get_field(field)
            dt['exists'].append(field)
        except FieldDoesNotExist:
            dt['absent'].append(field)
    return dt
