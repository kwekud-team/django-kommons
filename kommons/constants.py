from enum import Enum


class KommonsK(Enum):
    DATES_PERIOD_SECONDS = 'seconds'
    DATES_PERIOD_MINUTES = 'minutes'
    DATES_PERIOD_HOURS = 'hours'
    DATES_PERIOD_DAYS = 'days'
    DATES_PERIOD_WEEKS = 'weeks'
    DATES_PERIOD_MONTHS = 'months'
    DATES_PERIOD_YEARS = 'years'
